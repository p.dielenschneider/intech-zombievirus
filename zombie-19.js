/**
 * This structure representing a population is made from an array of people who each have the following properties:
 * @param {string} name - Name of the person
 * @param {number} age - Age of the person
 * @param {boolean} infected - Whether this person is infected or not
 * @param {boolean} dead - Whether this person is dead or not
 * @param {string | undefined} variant - Null if this person isn't infected, else a string containing the type of virus they were infected with
 * @param {string[]} immune - Array containing the virus identifiers this person is immune against
 * @param {person[]} acquaintances - An array containing "person" objects this person is related to
 */
const population = [
    {
        name: 'Dwight Schrute',
        age: 34,
        infected: false,
        dead: false,
        variant: null,
        immune: [],
        acquaintances: [
            {
                name: 'Jim Halpert',
                age: 29,
                infected: true,
                dead: false,
                variant: 'C',
                immune: [],
                acquaintances: [
                    {
                        name: 'Pam Halpert',
                        age: 28,
                        infected: false,
                        dead: false,
                        variant: null,
                        immune: [],
                        acquaintances: []
                    },
                    {
                        name: 'Phyllis Vance',
                        age: 41,
                        infected: false,
                        dead: false,
                        variant: null,
                        immune: [],
                        acquaintances: [
                            {
                                name: 'Bob Vance, Vance Refrigeration',
                                age: 40,
                                infected: false,
                                immune: [],
                                acquaintances: []
                            }
                        ]
                    },
                    {
                        name: 'Oscar Nuñez',
                        age: 31,
                        infected: false,
                        dead: false,
                        variant: null,
                        immune: [],
                        acquaintances: []
                    },
                ]
            },
            {
                name: 'Mose Schrute',
                age: 31,
                infected: false,
                dead: false,
                variant: null,
                immune: [],
                acquaintances: []
            }
        ]
    },
    {
        name: 'Michael Scott',
        age: 37,
        infected: false,
        dead: false,
        variant: null,
        immune: [],
        acquaintances: [
            {
                name: 'Robert California',
                age: 45,
                infected: false,
                dead: false,
                variant: null,
                immune: [],
                acquaintances: []
            },
            {
                name: 'David Wallace',
                age: 43,
                infected: false,
                dead: false,
                variant: null,
                immune: [],
                acquaintances: []
            }
        ]
    }
];

/**
 * A recursive function that spreads a virus with the provided infectionMode (superior function).
 * @param {person[]} population The population that the virus should spread in. This changes depending on where you are in the recursive loop.
 * @param {function} infectionMode The infection mode that should be used. Superior function, see ZombieA, ZombieB, Zombie32, ZombieC and ZombieU.
 * @param {boolean} parentInfected Whether the parent of this population group is infected.
 * @param {person} highestMember A reference to the highest member of this population group.
 * @returns {boolean} Whether one person of this group is infected.
 */
function zombieSpread(population, infectionMode, parentInfected = false, highestMember = null) {
    population.forEach(person => {
        const childInfected = zombieSpread(person.acquaintances, infectionMode, person.infected || parentInfected, highestMember === null ? person : highestMember);
        infectionMode(person, parentInfected, childInfected, highestMember);
    });
    return population.some(person => person.infected);
}

/**
 * A superior function passed to the zombieSpread() function to define the spread mode.
 * This one infects the person only if the parent was infected and if this person isn't immune to variant A.
 * @param {person} person A person from the population array.
 * @param {boolean} parentInfected Whether the parent of this person is infected.
 * @param {boolean} childInfected Whether the child of this person is infected.
 * @param {person} highestMember A reference to the highest member of this person's population group.
 */
const zombieA = function(person, parentInfected, childInfected, highestMember) {
    if (parentInfected && !person.immune.includes('A')) {
        console.log('Infected ' + person.name + ' with Zombie-A variant.');
        person.infected = true;
        person.variant = 'A';
    }
}

/**
 * A superior function passed to the zombieSpread() function to define the spread mode.
 * This one infects the person only if the child was infected and if this person isn't immune to variant B.
 * @param {person} person A person from the population array.
 * @param {boolean} parentInfected Whether the parent of this person is infected.
 * @param {boolean} childInfected Whether the child of this person is infected.
 * @param {person} highestMember A reference to the highest member of this person's population group.
 */
const zombieB = function(person, parentInfected, childInfected, highestMember) {
    if (childInfected && !person.immune.includes('B')) {
        console.log('Infected ' + person.name + ' with Zombie-B variant.');
        person.infected = true;
        person.variant = 'B';
    }
}

/**
 * A superior function passed to the zombieSpread() function to define the spread mode.
 * This one infects the person only if the parent or the child is infected, if the person is over 32 years old, and if the person isn't immune against variant 32.
 * @param {person} person A person from the population array.
 * @param {boolean} parentInfected Whether the parent of this person is infected.
 * @param {boolean} childInfected Whether the child of this person is infected.
 * @param {person} highestMember A reference to the highest member of this person's population group.
 */
const zombie32 = function(person, parentInfected, childInfected, highestMember) {
    if ((childInfected || parentInfected) && person.age >= 32 && !person.immune.includes('32')) {
        console.log('Infected ' + person.name + ' with Zombie-32 variant.');
        person.infected = true;
        person.variant = '32';
    }
}

/**
 * A superior function passed to the zombieSpread() function to define the spread mode.
 * This one infects one out of two people if they aren't immune against variant C. The other ones don't have any problems.
 * @param {person} person A person from the population array.
 * @param {boolean} parentInfected Whether the parent of this person is infected.
 * @param {boolean} childInfected Whether the child of this person is infected.
 * @param {person} highestMember A reference to the highest member of this person's population group.
 */
const zombieC = function(person, parentInfected, childInfected, highestMember) {
    if (person.infected) {
        person.acquaintances.forEach((acquaintance, index) => {
            if (index % 2 === 0 && !person.immune.includes('C')) {
                console.log('Infected ' + acquaintance.name + ' with Zombie-C variant.');
                acquaintance.infected = true;
                person.variant = 'C';
            }
        });
    }
}

/**
 * A superior function passed to the zombieSpread() function to define the spread mode.
 * This one infects the highest person of the group (in the original population array) if the provided person is positive (only if the highest member is not immune against variant U and isn't already infected).
 * @param {person} person A person from the population array.
 * @param {boolean} parentInfected Whether the parent of this person is infected.
 * @param {boolean} childInfected Whether the child of this person is infected.
 * @param {person} highestMember A reference to the highest member of this person's population group.
 */
const zombieU = function(person, parentInfected, childInfected, highestMember) {
    if (highestMember && !highestMember.immune.includes('U') && !highestMember.infected && person.infected) {
        console.log('Infected ' + highestMember.name + ' with Zombie-U variant.');
        highestMember.infected = true;
        highestMember.variant = 'U';
    }
}

/**
 * A recursive function that vaccinates member from the population array depending on their status. Uses a superior function to define the vaccination mode and conditions.
 * @param {person[]} population The population that the vaccine should spread in. This changes depending on where you are in the recursive loop.
 * @param {function} vaccineMode The vaccination mode that should be used. Superior function, see VaccineA1, VaccineB1 and VaccineU.
 */
function vaccineCampaign(population, vaccineMode) {
    population.forEach((person, index) => {
        vaccineMode(person, index);
        if (person.acquaintances.length === 0) {
            return;
        }
        vaccineCampaign(person.acquaintances, vaccineMode);
    });
}

/**
 * Superior function defining a vaccination mode, passed to the vaccineCampaign() function.
 * This one only vaccinates (and cures if needed) people under 30 who have variant A or 32.
 * It makes the person immune against all variants.
 * @param {*} person Person that will be tested to see if they can be vaccinated.
 * @param {*} index Current position of the person in the population array.
 */
const vaccineA1 = function(person, index) {
    if (person.age <= 30 && (person.variant === 'A' || person.variant === '32')) {
        console.log(person.name + ' is now immune against all variants.');
        person.immune = ['A', 'B', 'C', '32', 'U'];
        if (person.infected) {
            console.log(person.name + ' is now cured.');
            person.infected = false;
            person.variant = null;
        }
    }
}

/**
 * Superior function defining a vaccination mode, passed to the vaccineCampaign() function.
 * This one only vaccinates (and cures if needed) one out of two people if they are subject to variants B or C. The rest just die.
 * It does not provide any kind of immunity.
 * @param {*} person Person that will be tested to see if they can be vaccinated.
 * @param {*} index Current position of the person in the population array.
 */
const vaccineB1 = function(person, index) {
    if (index % 2 === 0) {
        if (person.infected && (person.variant === 'B' || person.variant === 'C')) {
            console.log(person.name + ' is now cured.');
            person.infected = false;
            person.variant = null;
        }
    } else {
        console.log(person.name + ' sadly died a horrible and painful death.');
        person.dead = true;
    }
}

/**
 * Superior function defining a vaccination mode, passed to the vaccineCampaign() function.
 * This one only vaccinates (and cures if needed) people who have the U variant.
 * It makes the person immune against all variants.
 * @param {*} person Person that will be tested to see if they can be vaccinated.
 * @param {*} index Current position of the person in the population array.
 */
const vaccineU = function(person, index) {
    if (person.variant === 'U') {
        console.log(person.name + ' is now immune against all variants.');
        person.immune = ['A', 'B', 'C', '32', 'U'];
        if (person.infected) {
            console.log(person.name + ' is now cured.');
            person.infected = false;
            person.variant = null;
        }
    }
}

zombieSpread(population, zombieA);
vaccineCampaign(population, vaccineA1);